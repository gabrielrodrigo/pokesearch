var btnSearch = document.getElementById('btnSearch');
var btnClear = document.getElementById('btnClear');
var online = navigator.onLine;

btnSearch.addEventListener('click', pokeSubmit);
btnClear.addEventListener('click', clearAll);

function pokeSubmit() {
    var name = document.getElementById('namePoke').value;

    if (name === "") {
        alert("Type a Pokémon name.");
    } else {
        var param = name.toLowerCase();
        var pokeURL = 'https://pokeapi.co/api/v2/pokemon/' + param;

        clearPokemon();

        if (online) {
            $.getJSON(pokeURL, function(data) {
                var jsonAux = JSON.stringify(data);
                var pokeImage = '<img class="materialboxed" width="150" src="' + data.sprites.front_default + '">';
                var pokeWeight = '<p>' + data.weight; + '</p>';
                var abilitySize = data.abilities.length;
                var statsSize = data.stats.length;
                var movesSize = data.moves.length;
                var typesSize = data.types.length;

                $('#pokeImage').append(pokeImage);

                //Show Poke Infos
                $('#lblAbilities').append('Poké Abilities:');

                for (var i = 0; i < abilitySize; i++) {
                    var pokeAbility = '<li class="collection-item">' + data.abilities[i].ability.name + '</li>';
                    $('#pokeAbilities').append(pokeAbility);
                }

                $('#lblWeight').append('Weight:');
                $('#pokeWeight').append(pokeWeight);

                $('#lblTypes').append('Poké Types:');
                for (var i = 0; i < typesSize; i++) {
                    var pokeTypes = '<li class="collection-item">' + data.types[i].type.name + '</li>';
                    $('#pokeTypes').append(pokeTypes);
                }

                //Show Poke Stats
                $('#lblStats').append('Poké Stats:');
                for (var i = 0; i < statsSize; i++) {
                    var pokeStat = '<li class="collection-item">' + data.stats[i].stat.name + ': ' + data.stats[i].base_stat + '</li>';
                    $('#pokeStats').append(pokeStat);
                }

                $('#lblMoves').append('Poké Moves:');
                for (var i = 0; i < movesSize; i++) {
                    var pokeMoves = '<li class="collection-item">' + data.moves[i].move.name + '</li>';
                    $('#pokeMoves').append(pokeMoves);
                }
            }).fail(function() {
                alert('Pokémon not found.');
            });
        } else {
            alert('Please check your connection.');
        }
    }
}

function clearPokemon() {
    $('#pokeImage').empty();
    $('#lblAbilities').empty();
    $('#pokeAbilities').empty();
    $('#lblWeight').empty();
    $('#pokeWeight').empty();
    $('#lblTypes').empty();
    $('#pokeTypes').empty();
    $('#lblStats').empty();
    $('#pokeStats').empty();
    $('#lblMoves').empty();
    $('#pokeMoves').empty();
}

function clearAll() {
    location.reload();
}