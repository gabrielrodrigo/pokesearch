var itensURL = 'https://pokeapi.co/api/v2/item/';
var itensLastURL = '';
var flag = false;

var btnNext = document.getElementById('btnNext');
var btnLast = document.getElementById('btnLast');
var online = navigator.onLine;

btnNext.addEventListener('click', listItens);
btnLast.addEventListener('click', listLastItens);

function listItens() {
    $('#itensName').empty();

    if (flag) {
        $('#btnLast').prop('disabled', false);
    }

    if (online) {
        $.getJSON(itensURL, function(data) {
            var size = data.results.length;

            for (var i = 0; i < size; i++) {
                $('#itensName').append('<li class="collection-item">' + data.results[i].name + '</li>');
            }

            itensURL = data.next;
            itensLastURL = data.previous;
            flag = true;
        }).fail(function() {
            alert('Nothing to show.');
            flag = false;
        });
    } else {
        alert('Please check your connection.');
    }

}

function listLastItens() {
    $('#itensName').empty();

    if (online) {
        $.getJSON(itensLastURL, function(data) {
            var size = data.results.length;

            for (var i = 0; i < size; i++) {
                $('#itensName').append('<li class="collection-item">' + data.results[i].name + '</li>');
            }

            itensURL = data.next;
            itensLastURL = data.previous;
        }).fail(function() {
            alert('Nothing to show.');
            flag = false;
        });
    } else {
        alert('Please check your connection.');
    }

}

listItens();