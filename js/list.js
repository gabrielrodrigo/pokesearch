var pokeURL = 'https://pokeapi.co/api/v2/pokemon/';
var pokeLastURL = '';
var flag = false;

var btnNext = document.getElementById('btnNext');
var btnLast = document.getElementById('btnLast');
var online = navigator.onLine;

btnNext.addEventListener('click', listPokemons);
btnLast.addEventListener('click', listLastPokes);

function listPokemons() {
    $('#list').empty();

    if (flag) {
        $('#btnLast').prop('disabled', false);
    }

    if (online) {
        $.getJSON(pokeURL, function(data) {
            var size = data.results.length;

            for (var i = 0; i < size; i++) {
                $('#list').append('<li class="collection-item">' + data.results[i].name + '</li>');
            }
            pokeURL = data.next;
            pokeLastURL = data.previous;
            flag = true;
        }).fail(function() {
            alert('Nothing to show.');
        });
    } else {
        alert('Please check your connection.');
    }
}

function listLastPokes() {
    $('#list').empty();

    if (online) {
        $.getJSON(pokeLastURL, function(data) {
            var size = data.results.length;

            for (var i = 0; i < size; i++) {
                $('#list').append('<li class="collection-item">' + data.results[i].name + '</li>');
            }
            pokeURL = data.next;
            pokeLastURL = data.previous;
        }).fail(function() {
            alert('Nothing to show.');
            flag = false;
        });
    } else {
        alert('Please check your connection.');
    }
}

listPokemons();